import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { UsersService } from './users.service';

import{RouterModule} from '@angular/router';

import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { AngularFireDatabase } from 'angularfire2/database-deprecated';
import { environment } from './../environments/environment';

import { DomSanitizer , SafeResourceUrl} from '@angular/platform-browser';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    UserComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path:'',component:MessagesComponent},
      {path: 'user/:id', component: UserComponent},
     {path:'login',component:LoginComponent}
    ])
  ],
  providers: [
    UsersService , 
    AngularFireDatabase],
  bootstrap: [AppComponent]
})
export class AppModule { }
