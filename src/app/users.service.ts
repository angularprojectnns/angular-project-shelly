import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import {environment } from './../environments/environment';
import { AngularFireDatabase} from 'angularfire2/database';

@Injectable()
export class UsersService {
  http:Http;
  token;

  getMessagesFire(){
    //if(this.token){
    return this.db.list('/messages').snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });
  //}
  }
   
  getCommentsFire(){
    return this.db.list('/comments').valueChanges();
  } 

  putPopular(data, id){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('popular',data);
    return this.http.put('http://localhost/project/slim/users/'+ id ,params.toString(), options);
  }

  //Get image
  getImage(id){
    return this.http.get('http://localhost/project/slim/users/image/'+ id);
  }

  /*getImage(imageUrl: string){
    return this.http
        .get(imageUrl, { responseType: ResponseContentType.Blob })
        .map((res: Response) => res.blob());
  }*/

  getUser(id){
    let options =  {
      headers:new Headers({
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
        'Token': this.token    
      })
    }
    return this.http.get('http://localhost/project/slim/users/'+ id, options);
  }

  getUsers(){
    return this.http.get('http://localhost/project/slim/users');
  }

  login(credentials){
    let options = {
       headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
       })
    }
   let  params = new HttpParams().append('username', credentials.user).append('password',credentials.password);
   return this.http.post('http://localhost/project/slim/login', params.toString(),options).map(response=>{ 
     let token = response.json().token;
     if (token){
       localStorage.setItem('token', token);
       console.log(token);
     }
  });
 }


  constructor(http:Http, private db: AngularFireDatabase) {
    this.http = http;
    this.token = localStorage.getItem('token');
   }

}
